## Some links to useful webpages

### GGPlots
http://r-statistics.co/Top50-Ggplot2-Visualizations-MasterList-R-Code.html#5.%20Composition


### HTML Tables in R
https://cran.r-project.org/web/packages/kableExtra/vignettes/awesome_table_in_html.html

### LavaanPlot
https://cran.r-project.org/web/packages/lavaanPlot/vignettes/Intro_to_lavaanPlot.html

http://rich-iannone.github.io/DiagrammeR/graphviz_and_mermaid.html#node-attributes

http://rich-iannone.github.io/DiagrammeR/graphviz_and_mermaid.html#edge-attributes
